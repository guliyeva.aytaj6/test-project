import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class D_RateFinishedOrder {

    protected AndroidDriver<AndroidElement> driver = null;
    DesiredCapabilities dc = new DesiredCapabilities();

    @Before
    public void setUp() throws MalformedURLException {
        dc.setCapability("deviceName","emulator-5554");
        dc.setCapability("platformName", "Android");
        dc.setCapability("appPackage", "az.crbn.delivery189");
        dc.setCapability("appActivity", ".MainActivity");
        dc.setCapability("autoGrantPermissions", "true");
        String reportDirectory = "reports";
        dc.setCapability("reportDirectory", reportDirectory);
        String reportFormat = "xml";
        dc.setCapability("reportFormat", reportFormat);
        String testName = "Restaurant_App_Test";
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "");
        driver = new AndroidDriver<>(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void Courier_App_Test() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click to the | CHOOSE A RESTAURANT | button
        driver.findElement(By.xpath("//*[@text='CHOOSE A RESTAURANT']")).click();
        Thread.sleep(8000);
        // configure the scrolling
        Dimension dimensions = driver.manage().window().getSize();
        double screenHeightStart = dimensions.getHeight() * 0.5;
        int scrollStart = (int) screenHeightStart;
        double screenHeightEnd = dimensions.getHeight() * 0.2;
        int scrollEnd = (int) screenHeightEnd;
        // Login to App
        MobileElement threeDotsBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/drawerImage");
        threeDotsBtn.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Click Login
        MobileElement loginBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
        loginBtn.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Select country code
        MobileElement countryCodeBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/selected_country_tv");
        countryCodeBtn.click();
        // Send country code
        MobileElement countryCodeFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/search_edt");
        countryCodeFld.click();
        countryCodeFld.sendKeys("az");
        // Select the country from the list
        MobileElement selectedCountryLI = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.TextView[1]");
        selectedCountryLI.click();
        // Send mobile number
        MobileElement mobileNumberFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/number");
        mobileNumberFld.click();
        mobileNumberFld.sendKeys("515355425");
        // Agree terms and conditions
        MobileElement termsAndConditionsBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/terms");
        termsAndConditionsBtn.click();
        // Click the Next button
        MobileElement nextBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/nextButton");
        nextBtn.click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        // Enter the OTP code
        MobileElement OTPCodeFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/code");
        OTPCodeFld.click();
        driver.hideKeyboard();
        OTPCodeFld.sendKeys("123456");
        // Click the Next Button
        MobileElement nextBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/nextButton");
        nextBtn1.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click the menu button
        MobileElement threeDotsBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/drawerImage");
        threeDotsBtn1.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Click | My Orders | Section
        AndroidElement myOrdersBtn = driver.findElement(By.xpath("//*[@text='My orders']"));
        myOrdersBtn.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click | Rate | Button
        AndroidElement rateBtn = driver.findElement(By.xpath("(//*[@id='recyclerView']/*/*[@text='Rate'])[1]"));
        rateBtn.click();
        // Click | Star | Button
        driver.findElement(By.xpath("//*[@id='rating']")).click();
    }

    private void login() {
        // Click Login
        MobileElement loginBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
        loginBtn.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Select country code
        MobileElement countryCodeBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/selected_country_tv");
        countryCodeBtn.click();
        // Send country code
        MobileElement countryCodeFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/search_edt");
        countryCodeFld.click();
        countryCodeFld.sendKeys("az");
        // Select the country from the list
        MobileElement selectedCountryLI = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.TextView[1]");
        selectedCountryLI.click();
        // Send mobile number
        MobileElement mobileNumberFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/number");
        mobileNumberFld.click();
        mobileNumberFld.sendKeys("515355425");
        // Agree terms and conditions
        MobileElement termsAndConditionsBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/terms");
        termsAndConditionsBtn.click();
        // Click the Next button
        MobileElement nextBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/nextButton");
        nextBtn.click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        // Enter the OTP code
        MobileElement OTPCodeFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/code");
        OTPCodeFld.click();
        driver.hideKeyboard();
        OTPCodeFld.sendKeys("123456");
        // Click the Next Button
        MobileElement nextBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/nextButton");
        nextBtn1.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
