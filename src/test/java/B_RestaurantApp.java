import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class B_RestaurantApp {

    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();

    @Before
    public void setUp() throws MalformedURLException {
        dc.setCapability("deviceName","emulator-5554");
        dc.setCapability("platformName", "Android");
        dc.setCapability("appPackage", "az.delivery189.restaurant");
        dc.setCapability("appActivity", ".ui.activities.StartActivity");
        dc.setCapability("autoGrantPermissions", "true");
        String reportDirectory = "reports";
        dc.setCapability("reportDirectory", reportDirectory);
        String reportFormat = "xml";
        dc.setCapability("reportFormat", reportFormat);
        String testName = "Restaurant_App_Test";
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "");
        driver = new AndroidDriver<>(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }


    @Test
    public void Restaurant_App_Test() throws InterruptedException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        MobileElement el1 = (MobileElement) driver.findElementById("az.delivery189.restaurant:id/username");
        el1.click();
        driver.hideKeyboard();
        el1.sendKeys("cadoro");
        MobileElement el2 = (MobileElement) driver.findElementById("az.delivery189.restaurant:id/password");
        el2.click();
        driver.hideKeyboard();
        el2.sendKeys("test123");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        MobileElement el3 = (MobileElement) driver.findElementById("az.delivery189.restaurant:id/nextButton");
        el3.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        MobileElement el4 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup");
        el4.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        MobileElement el5 = (MobileElement) driver.findElementById("az.delivery189.restaurant:id/acceptButton");
        el5.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        MobileElement el6 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.TextView");
        el6.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        MobileElement el7 = (MobileElement) driver.findElementById("az.delivery189.restaurant:id/sendButton");
        el7.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        MobileElement el8 = (MobileElement) driver.findElementById("az.delivery189.restaurant:id/acceptButton");
        el8.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

    }



    @After
    public void tearDown() {
        driver.quit();
    }

}
