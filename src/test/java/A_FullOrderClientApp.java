import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.IntStream;

public class A_FullOrderClientApp {

    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();

    @Before
    public void setUp() throws MalformedURLException {
        dc.setCapability("deviceName","emulator-5554");
        dc.setCapability("platformName", "Android");
        dc.setCapability("appPackage", "az.crbn.delivery189");
        dc.setCapability("appActivity", ".MainActivity");
        // az.crbn.delivery189/az.crbn.delivery189.MainActivity
        dc.setCapability("autoGrantPermissions", "true");
        String reportDirectory = "reports";
        dc.setCapability("reportDirectory", reportDirectory);
        String reportFormat = "xml";
        dc.setCapability("reportFormat", reportFormat);
        String testName = "Full_Order_Small";
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "");
        driver = new AndroidDriver<>(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testFullOrderBeta() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click to the | CHOOSE A RESTAURANT | button
        driver.findElement(By.xpath("//*[@text='CHOOSE A RESTAURANT']")).click();
        Thread.sleep(8000);
        // configure the scrolling
        Dimension dimensions = driver.manage().window().getSize();
        double screenHeightStart = dimensions.getHeight() * 0.5;
        int scrollStart = (int) screenHeightStart;
        double screenHeightEnd = dimensions.getHeight() * 0.2;
        int scrollEnd = (int) screenHeightEnd;
        // Click the menu button
        MobileElement threeDotsBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/drawerImage");
        threeDotsBtn.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Click Login
        MobileElement loginBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
        loginBtn.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Select country code
        MobileElement countryCodeBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/selected_country_tv");
        countryCodeBtn.click();
        // Send country code
        MobileElement countryCodeFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/search_edt");
        countryCodeFld.click();
        countryCodeFld.sendKeys("az");
        // Select the country from the list
        MobileElement selectedCountryLI = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.TextView[1]");
        selectedCountryLI.click();
        // Send mobile number
        MobileElement mobileNumberFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/number");
        mobileNumberFld.click();
        mobileNumberFld.sendKeys("515355425");
        // Agree terms and conditions
        MobileElement termsAndConditionsBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/terms");
        termsAndConditionsBtn.click();
        // Click the Next button
        MobileElement nextBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/nextButton");
        nextBtn.click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        // Enter the OTP code
        MobileElement OTPCodeFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/code");
        OTPCodeFld.click();
        driver.hideKeyboard();
        OTPCodeFld.sendKeys("123456");
        // Click the Next Button
        MobileElement nextBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/nextButton");
        nextBtn1.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click the menu
        MobileElement threeDotsBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/drawerImage");
        threeDotsBtn1.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Click the Payment Methods
        MobileElement paymentMethodBtn = (MobileElement) driver.findElementByXPath("//*[@text='Payment methods']");
        paymentMethodBtn.click();
        // Click add card
        MobileElement addCardBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.widget.LinearLayout/android.widget.TextView");
        addCardBtn.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click the back to Payment Methods button
        MobileElement backBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageButton");
        backBtn.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click Visa Card to select
        MobileElement visaCardBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]");
        visaCardBtn.click();
        // Click to get the menu
        MobileElement menuBtn1 = (MobileElement) driver.findElementByXPath("//*[@class='android.widget.ImageButton']");
        menuBtn1.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click the menu button
        MobileElement threeDotsBtn2 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/drawerImage");
        threeDotsBtn2.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Click the My Addresses button
        MobileElement myAddressesBtn = (MobileElement) driver.findElementByXPath("//*[@text='My addresses']");
        myAddressesBtn.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click Add New Address button
        MobileElement addNewAddressBtn = (MobileElement) driver.findElementByXPath("//*[@text='Add new address']");
        addNewAddressBtn.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Generate random Title
        MobileElement titleForNewLocationLbl = (MobileElement) driver.findElementById("az.crbn.delivery189:id/title");
        titleForNewLocationLbl.sendKeys("Appium Bot House | ID: " + Math.random());
        // Enter the Entrance
        MobileElement enteranceNumberLbl = (MobileElement) driver.findElementById("az.crbn.delivery189:id/entrance");
        enteranceNumberLbl.click();
        enteranceNumberLbl.sendKeys("1");
        // Enter the Floor
        MobileElement floorNumberLbl = (MobileElement) driver.findElementById("az.crbn.delivery189:id/floor");
        floorNumberLbl.click();
        floorNumberLbl.sendKeys("1");
        // Enter the Apartment
        MobileElement apartmentNumberLbl = (MobileElement) driver.findElementById("az.crbn.delivery189:id/apartment");
        apartmentNumberLbl.click();
        apartmentNumberLbl.sendKeys("1");
        // Enter new Comment
        MobileElement commentFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/comment");
        commentFld.click();
        commentFld.sendKeys("Hopefully it's gonna work properly ;)");
        // Add new Location
        MobileElement completeAddingNewLocationBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/add_button");
        completeAddingNewLocationBtn.click();
        // Click to get back the main page
        MobileElement backToMainPageBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageButton");
        backToMainPageBtn.click();
        // click | Hard Rock | image
        driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView[3]/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ImageView")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click | Product | image
        driver.findElementById("az.crbn.delivery189:id/productImage").click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click | Plus | button
        IntStream.range(0, 5).forEach(i -> driver.findElementById("az.crbn.delivery189:id/increase").click());
        // click | Add | button
        driver.findElementById("az.crbn.delivery189:id/add").click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click | Basket | button
        driver.findElement(By.xpath("//*[@text='Basket']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Click the Next button
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Next']")));
        driver.findElement(By.xpath("//*[@text='Next']")).click();
        // Click the Address field
        MobileElement addressFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/addressLayout");
        addressFld.click();
        // Change the address
        MobileElement address1 = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
        address1.click();
        // Change back to default address
        MobileElement address2 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/addressText");
        address2.click();
        MobileElement chooseAddressBtn = (MobileElement) driver.findElementByXPath("//*[@text='work']");
        chooseAddressBtn.click();
        //  Changing the delivery type
        MobileElement deliveryTypeBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/deliveryTypeLayout");
        deliveryTypeBtn.click();
        MobileElement atTableBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/atTableText");
        atTableBtn.click();
        MobileElement deliveryTypeBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/deliveryTypeLayout");
        deliveryTypeBtn1.click();
        MobileElement defaultDeliveryType = (MobileElement) driver.findElementById("az.crbn.delivery189:id/deliveryText");
        defaultDeliveryType.click();
        // Changing Time
        MobileElement changeTimeBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/timeLayout");
        changeTimeBtn.click();
        // Choose the time
        MobileElement newTimeBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/dateLayout");
        newTimeBtn.click();
        // Click the Done button
        MobileElement doneButton = (MobileElement) driver.findElementById("az.crbn.delivery189:id/doneButton");
        doneButton.click();
        MobileElement changeTimeBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189:id/timeLayout");
        changeTimeBtn1.click();
        // Click as soon button
        MobileElement asSoonAsPossibleBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/soonPossibleText");
        asSoonAsPossibleBtn.click();
        // Click done button
        MobileElement doneBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/doneButton");
        doneBtn.click();
        // Change payment type
        MobileElement paymentTypeBtn = (MobileElement) driver.findElementById("az.crbn.delivery189:id/paymentLayout");
        paymentTypeBtn.click();
        MobileElement chooseCashBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]");
        chooseCashBtn.click();
        // Use cash calculator
        MobileElement cashCalcFld = (MobileElement) driver.findElementById("az.crbn.delivery189:id/calculatorCash");
        cashCalcFld.click();
        cashCalcFld.sendKeys("100");
        // Change payment type back to default
        paymentTypeBtn.click();
        MobileElement defaultPaymentMethodBtn = (MobileElement) driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.TextView");
        defaultPaymentMethodBtn.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Complete the order
        MobileElement completeOrderBtn = (MobileElement) driver.findElementByXPath("//*[@text='Confirm order']");
        completeOrderBtn.click();
    }

    @After
    public void tearDown() {
        driver.quit();
    }


}
