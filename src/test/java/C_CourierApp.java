import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class C_CourierApp {

    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();

    @Before
    public void setUp() throws MalformedURLException {
        dc.setCapability("deviceName","emulator-5554");
        dc.setCapability("platformName", "Android");
        dc.setCapability("appPackage", "az.crbn.delivery189.courier");
        dc.setCapability("appActivity", ".MainActivity");
        dc.setCapability("autoGrantPermissions", "true");
        String reportDirectory = "reports";
        dc.setCapability("reportDirectory", reportDirectory);
        String reportFormat = "xml";
        dc.setCapability("reportFormat", reportFormat);
        String testName = "Restaurant_App_Test";
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "");
        driver = new AndroidDriver<>(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void Courier_App_Test() throws InterruptedException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Select country code
        MobileElement countrySelectBtn = driver.findElementById("az.crbn.delivery189.courier:id/selected_country_tv");
        countrySelectBtn.click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // Search for Azerbaijan
        MobileElement searchTxtBox = driver.findElementById("az.crbn.delivery189.courier:id/search_edt");
        searchTxtBox.click();
        searchTxtBox.sendKeys("az");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // Select country
        MobileElement countryFlagBtn = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.TextView[1]");
        countryFlagBtn.click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // Enter the number
        MobileElement phoneNumberTxtBox = driver.findElementById("az.crbn.delivery189.courier:id/number");
        phoneNumberTxtBox.click();
        phoneNumberTxtBox.sendKeys("515355425");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // Click the next button
        MobileElement nextBtn = driver.findElementById("az.crbn.delivery189.courier:id/button");
        nextBtn.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // Enter OTP code
        MobileElement otpTxtBox = driver.findElementById("az.crbn.delivery189.courier:id/code");
        otpTxtBox.click();
        Thread.sleep(5000);
        otpTxtBox.sendKeys("123456");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        // Click the next button
        MobileElement nextBtn1 = (MobileElement) driver.findElementById("az.crbn.delivery189.courier:id/button");
        nextBtn1.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Navbar testing
        MobileElement timeTableBtn = (MobileElement) driver.findElementById("az.crbn.delivery189.courier:id/navTimeTableIcon");
        timeTableBtn.click();
        MobileElement historyBtn = (MobileElement) driver.findElementById("az.crbn.delivery189.courier:id/navHistory");
        historyBtn.click();
        MobileElement chatIconBtn = (MobileElement) driver.findElementById("az.crbn.delivery189.courier:id/navChatIcon");
        chatIconBtn.click();
        MobileElement homeIconBtn = (MobileElement) driver.findElementById("az.crbn.delivery189.courier:id/navHomeIcon");
        homeIconBtn.click();
        // Click on the profile image
        MobileElement imageBtn = (MobileElement) driver.findElementById("az.crbn.delivery189.courier:id/courierImage");
        imageBtn.click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        // Click | upcoming order |
        MobileElement upcomingOrderBtn = (MobileElement) driver.findElementByXPath("//*[@class='android.view.ViewGroup' and ./*[@text='ID 728']]");
        upcomingOrderBtn.click();

    }



    @After
    public void tearDown() {
        driver.quit();
    }

}
